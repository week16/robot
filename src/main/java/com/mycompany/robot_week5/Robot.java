/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robot_week5;

/**
 *
 * @author nymr3kt
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private Map tableMap;

    public Robot(char symbol, int x, int y, Map tableMap) {
        this.symbol = symbol;
        this.x = x;
        this.y = y;
        this.tableMap = tableMap;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;

    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N': 
            case 'w' :
                if (walkToN()) return false;
                break;
            case 'S': 
            case 's':
                
                if (walkToS()) return false;
                break;
            case 'E':
            case 'd' :
                if (walkToE()) return false;
                break;
            case 'W':
            case 'a':
                if (walkToW()) return false;
                break;
            default:
                return false ;

        } System.out.println("Positoin : (" + x + ',' + y + ')');
        
        if(tableMap.isBomb(x, y)){
            System.out.println("Found Bomb!!! : (" + x + ',' + y + ')');
        }
        return true;
    }

    private boolean walkToW() {
        if (tableMap.inMap(x - 1, y)) {
            x = x - 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkToE() {
        if (tableMap.inMap(x + 1, y)) {
            x = x + 1;;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkToS() {
        if (tableMap.inMap(x, y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkToN() {
        if (tableMap.inMap(x, y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
        return false;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }

}
